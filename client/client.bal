import ballerina/http;
import ballerina/io;

final http:Client clientEndpoint = check new ("http://localhost:4500");
string result  = "";
public function main() returns error? {


    while (true) {
        
        io:println("*********************************");
        io:println("*** PROBLEM 3 ***");
        io:println("*********************************");
        io:println("1. Create student");
        io:println("2. Update student ");
        io:println("3. Update student's course ");
        io:println("4. Get a single student");
        io:println("5. fetch all students");
        io:println("6. Delete student");
        io:println("*********************************");

        string choice = io:readln("Enter choice 1-6: ");

        match choice {

            "1" => {
                 create_student();
            }

            "2" => {
                    update_student();
            }

            "3" => {
                   update_course_details();
            }

            "4" => {
                    lookup_student_details ();
            }
            "5" => {
                     all_students();
            }
            "6" => {
                     delete_student();
            }

            _ => {
                  io:println( "oops  error ");
            }
        }
    }
}

function create_student() {
                string id = io:readln("Enter student number: ");
                string name = io:readln("Enter name: ");
                string email = io:readln("Enter email: ");
                string address = io:readln("Enter address: ");

                 result = checkpanic clientEndpoint->post("/problem3/createStudent", {studentNumber: id, studentName: name, studentEmail: email, studentAddress: address});
                io:println(result);
}

function update_student() {
                    string studentNumber = io:readln("Enter student number: ");

                    io:println("Enter new value ");
                    string name = io:readln("Enter name: ");
                    string email = io:readln("Enter email: ");
                    string address = io:readln("Enter address: ");

                     result = checkpanic clientEndpoint->put("/problem3/update_student", {studentNumber: studentNumber, studentName: name, studentEmail: email, studentAddress: address});
                    io:println(result.toJsonString());
}

function update_course_details() {
                string studentNumber = io:readln("Enter student number: ");
                string course = io:readln("Enter course code: ");

                 result = checkpanic clientEndpoint->put("/problem3/create_course", {studentNumber: studentNumber, course: course});
                io:println(result.toJsonString());
}

function lookup_student_details () {
                string studentNumber = io:readln("Enter student number: ");

                json result = checkpanic clientEndpoint->get("/problem3/lookupStudent?studentNumber=" + studentNumber);
                io:println(result);
}

function all_students() {
                        json result = checkpanic clientEndpoint->get("/problem3/all_students");
                    io:println(result);
}

function delete_student() {
                string studentNumber = io:readln("Enter student number: ");
                 result = checkpanic clientEndpoint->delete("/problem3/delete_students?studentNumber=" + studentNumber);
                io:println(result);
}