import ballerina/http;


service /problem3 on new http:Listener(4500) {
    
    resource function put create_course(@http:Payload requestCourse course_details) returns string {

        Student student = students.get(course_details.studentNumber);
        student.courses.push(course_details.course);

        return "new course added";
    }

    resource function put update_student(@http:Payload StudentRequest student_details) returns string {

        Student student = students.get(student_details.studentNumber);
        

        if (student_details.address !== "") {
            student.studentAddress = student_details.address;
        }

        if (student_details.email !== "") {
            student.studentEmail = student_details.email;

        }

        if (student_details.name !== "") {
            student.studentName = student_details.name;
        }

        return "Updated student details";

    }



    resource function get lookupStudent(string studentNumber) returns Student {
        return students.get(studentNumber);
    }

    resource function get all_students() returns json {
        return students.toJson();
    }

    resource function delete delete_students(string studentNumber) returns string {
        Student result = students.remove(studentNumber);

        return "Student has been deleted";

    }

        resource function post createStudent(@http:Payload Student student_details) returns string {
       
        students.add({   studentNumber: student_details.studentNumber,  studentName: student_details.studentName,  studentEmail: student_details.studentEmail,studentAddress: student_details.studentAddress, courses: []});

        return  "Student created successfully.";
    }
}

public type Student record {|
    readonly string studentNumber;
    string studentEmail;
    string studentName;
    string studentAddress;
    string[] courses = [];
|};

public type Submission record {|
    readonly string studentNumber;
    float mark;
    string content;
|};

public type Assignment record {|
    readonly string id;
    float weight;
    table<Submission> submissions;
|};

public type Course record {|
    readonly string code;
    table<Assignment> assignments;
|};

table<Student> key(studentNumber) students = table [ ];

table<Course> key(code) courses = table [ ];

public type StudentRequest record {|
    string studentNumber;
    string email = "";
    string name = "";
    string address = "";
|};

public type requestCourse record {|
    string studentNumber;
    string course;
|};

